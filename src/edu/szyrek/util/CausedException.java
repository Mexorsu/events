package edu.szyrek.util;

public class CausedException extends Exception {
    static final long serialVersionUID = 21364572351L;
    private String rootCause;
    private String fullCause;

    public CausedException(Exception e){
        rootCause = e.getLocalizedMessage();
        StringBuffer sb = new StringBuffer(rootCause);
        for (Throwable cause = e.getCause(); cause != null; cause = cause.getCause()){
            sb.append(", caused by:\n"+cause.getLocalizedMessage());
            rootCause = e.getLocalizedMessage();
        }
        fullCause = sb.toString();
    }
    public String getRootCause() {
        return this.rootCause;
    }
    public String getFullCause(){
        return this.fullCause;
    }
}

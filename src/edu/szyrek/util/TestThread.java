package edu.szyrek.util;

public class TestThread extends Thread {
    public final static long SLEEP_INTERVAL = 1000L;
    public String name;
    public TestThread(String name){
        this.name = name;
    }
    @Override
    public void run(){
        System.out.println("Thread "+name+" started working");
        try {
            sleep(SLEEP_INTERVAL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Thread "+name+" finished working");
    }
}


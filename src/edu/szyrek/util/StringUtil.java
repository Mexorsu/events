package edu.szyrek.util;

public class StringUtil {
    private StringUtil(){}
    private static class StringUtilLoader {
        public static final StringUtil INSTANCE = new StringUtil();
    }
    public static StringUtil getInstance(){
        return StringUtilLoader.INSTANCE;
    }
    public void printLine() {
        System.out.println("--------------------------");
    }
}

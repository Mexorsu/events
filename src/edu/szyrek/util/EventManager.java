package edu.szyrek.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class EventManager {
    private static class EventManagerLazyLoader {
        public static final EventManager INSTANCE = new EventManager();
    }
    public static EventManager getInstance() {
        return EventManagerLazyLoader.INSTANCE;
    }
    private ConcurrentHashMap<Class<? extends Event>, List<EventConsumer<? extends Event>>> consumers = new ConcurrentHashMap<Class<? extends Event>, List<EventConsumer<? extends Event>>>();
    private EventManager() {}
    
    public <T extends Event> void registerListener(Class<? extends Event> clazz, EventConsumer<? extends Event> consumer){
        if (this.consumers.get(clazz)==null){
            this.consumers.put(clazz, new ArrayList<>());
        }
        this.consumers.get(clazz).add(consumer);
    }
    
    public <T extends Event> void handleEvent(T event){
        //T cast = (T) event;
        if (consumers.get(event.getClass())==null || consumers.get(event.getClass()).size()==0){
            return;
        }
        for (EventConsumer consumer: consumers.get(event.getClass())){
            consumer.consume(event);
        }
    }
}

package edu.szyrek.util;

public abstract class EventProducer {
	private final EventManager manager = EventManager.getInstance();
	protected final <T extends Event> void fireEvent(T e){
		manager.handleEvent(e);
	}
}

package edu.szyrek.util;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import edu.szyrek.threadfun.entrypoints.MyBlockingQueue;

interface EventHandler<T extends Event> {
	public void handle(T e);
}

class MyBlockingQueueueWithASAP<T> extends MyBlockingQueue<T> {
	public MyBlockingQueueueWithASAP(int arg0) {
		super(arg0);
	}
	ExecutorService exec = Executors.newFixedThreadPool(4);
	class Adder extends Thread {
		T elem;
		public Adder(T elem){
			this.elem = elem;
		}
		@Override
		public void run(){
			try {
				put(elem);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	// Same as put, but if queue is full,
	// then it spawns a new Thread which 
	// does all the waiting while caller
	// carries on
	public void putASAP(T elem){
		try { 
			this.add(elem);
		}catch (IllegalStateException e){
			exec.execute(new Adder(elem));
		}
	}
}

class ArrayBlockingQueueueWithASAP<T> extends ArrayBlockingQueue<T> {
	public ArrayBlockingQueueueWithASAP(int arg0) {
		super(arg0);
	}
	ExecutorService exec = Executors.newFixedThreadPool(4);
	class Adder extends Thread {
		T elem;
		public Adder(T elem){
			this.elem = elem;
		}
		@Override
		public void run(){
			try {
				put(elem);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	// Same as put, but if queue is full,
	// then it spawns a new Thread which 
	// does all the waiting while caller
	// carries on
	public void putASAP(T elem){
		try { 
			this.add(elem);
		}catch (IllegalStateException e){
			exec.execute(new Adder(elem));
		}
	}
}

public abstract class EventConsumer<T extends Event> implements EventHandler<T> {
	private MyBlockingQueueueWithASAP<T> eventQueue = new MyBlockingQueueueWithASAP<T>(10);
	private boolean running = true;	
	
	Thread eventThread = new Thread(){
		@Override
		public void run() {
			while (running||!eventQueue.isEmpty()){
				try {
					T event = eventQueue.take();
					handle(event);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	};
	
	public void shutDown(boolean immediate) {
		if (immediate){
			eventQueue.clear();
		}
		running = false;
		eventQueue.exec.shutdown();
	}
	
	final public void consume(T event){
		try {
			if (running){
				eventQueue.putASAP(event);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public EventConsumer() {
		eventThread.start();
	}
	
}

package edu.szyrek.threadfun.entrypoints;

import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import edu.szyrek.util.Event;
import edu.szyrek.util.EventConsumer;
import edu.szyrek.util.EventManager;
import edu.szyrek.util.EventProducer;

class TestEvent extends Event {
	private int number;
	
	public int getNumber() {
		return number;
	}
	public TestEvent(int num) {
		this.number = num;
	}
	public String toString(){
		return "test event#"+number;
	}
	
}
class BackFireEvent extends Event {
	private int number;
	public BackFireEvent(int num) {
		this.number = num;
	}
	public String toString(){
		return "backfire event#"+number;
	}
	
}
class OrphanEvent extends Event {
	public String toString(){
		return "i will not print anyway, no handlers registered for meh :(";
	}
	
}

class TestEventPrinter extends EventProducer {
	Event current;
	Lock currentLock = new ReentrantLock();
	
	EventConsumer<TestEvent> testConsumer = new EventConsumer<TestEvent>() {
		@Override
		public void handle(TestEvent event) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				// locking here because we access
				// "current" from two consumers at the 
				// same time, and each consumer will be
				// running on his own thread
				currentLock.lock();
				current = event;
				print();
			}catch (Exception e){
				e.printStackTrace();
			}finally{
				currentLock.unlock();	
			}
			BackFireEvent resp = new BackFireEvent(event.getNumber());
			fireEvent(resp);
		}
	};
	EventConsumer<BackFireEvent> otherConsumer = new EventConsumer<BackFireEvent>() {
		@Override
		public void handle(BackFireEvent event) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				currentLock.lock();
				current = event;
				print();
			}catch (Exception e){
				e.printStackTrace();
			}finally{				
				currentLock.unlock();
                TestEvents.mainLatch.countDown();
			}
		}
	};
	public void print() {
		System.out.println("Handling event "+current.toString());
	}

}

public class TestEvents extends EventProducer {
	
    public static CountDownLatch mainLatch;
	public static void test() {
		OrphanEvent ee = new OrphanEvent();
		
		TestEventPrinter printer = new TestEventPrinter();
		EventManager.getInstance().registerListener(TestEvent.class, printer.testConsumer);
		EventManager.getInstance().registerListener(BackFireEvent.class, printer.otherConsumer);
		TestEvents context = new TestEvents();
        Scanner scaner = new Scanner(System.in);
        System.out.println("How many events to fire?");
        Integer count = scaner.nextInt();
        TestEvents.mainLatch = new CountDownLatch(count);
		for (int i = 0; i < count; i++) {
			context.fireEvent(new TestEvent(i));	
		}
		System.out.println("Finished firing!");		
		try {
            TestEvents.mainLatch.await();
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
        System.out.println("All events handled");
		printer.testConsumer.shutDown(false);
		printer.otherConsumer.shutDown(false);
	}
}

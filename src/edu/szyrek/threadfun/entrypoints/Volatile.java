package edu.szyrek.threadfun.entrypoints;

import java.util.Scanner;

class Processor extends Thread {
    private volatile boolean running = true;
    @Override
    public void run() {
        while (running) {
            System.out.println("Hello");
            try {
                Thread.sleep(100);           
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }    
    public void shutdown(){
        running = false;
    }
}
public class Volatile {
    public static void test () {
        Processor p1 = new Processor();
        p1.start();

        System.out.println("Press <enter> to quit");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        p1.shutdown();
        input.close();
    }
}

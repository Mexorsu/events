package edu.szyrek.threadfun.entrypoints;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class TestLocks {
    private static List<Integer> list1 = new ArrayList<>();
    private static Object lock1 = new Object();
    private static Object lock2 = new Object();
    private static List<Integer> list2 = new ArrayList<>();

    private static Random r = new Random(System.nanoTime());

    public static void stage1(){
    	synchronized (lock1){
	    	try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO: handle exception
			}
    		list1.add(r.nextInt(100));
    	}
    }
    
    public static void stage2(){
    	synchronized(lock2){
	    	try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO: handle exception
			}
    		list2.add(r.nextInt(100));
    	}
    }
    
    public static void process() {
    	for (int i = 0; i < 1000; i++) {
			stage1();
			stage2();
		}
    }
    
    public static void test () {
    	long start = System.currentTimeMillis();
    	System.out.println("--- TEST LOCKS: ---");
    	Thread t1 = new Thread(){
    		public void run(){
    			process();
    		}
    	};
    	Thread t2 = new Thread(){
    		public void run(){
    			process();
    		}
    	};
    	t1.start();
    	t2.start();
    	try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	System.out.println("It took "+(System.currentTimeMillis()-start));
    	System.out.println("List1: "+list1.size());
    	System.out.println("List2: "+list2.size());
    }
}

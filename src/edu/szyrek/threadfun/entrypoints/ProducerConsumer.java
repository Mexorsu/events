package edu.szyrek.threadfun.entrypoints;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ProducerConsumer {
	private static BlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(100);
	private static Random r = new Random(System.currentTimeMillis());
	private static Thread producer = new Thread(){
		public void run(){
			while(true){
				try {
					Thread.sleep(80);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				queue.add(r.nextInt(1000));
				//System.out.println("Adding");
			}
		}
	};
	private static Thread consumer = new Thread(){
		public void run(){
			while(true){
				try {
					Thread.sleep(85);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println("Removing");
				try {
					Integer val = queue.take();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	};
	public static void test(){
		producer.start();
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		consumer.start();
		while (true){
			System.out.println(queue.size());
		}
		
	}
	
}

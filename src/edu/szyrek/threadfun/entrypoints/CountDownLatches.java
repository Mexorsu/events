package edu.szyrek.threadfun.entrypoints;

import java.util.Random;
import java.util.concurrent.CountDownLatch;


public class CountDownLatches {
	static CountDownLatch latch = new CountDownLatch(10);
	static Random r = new Random(System.nanoTime());
	static class Minion extends Thread{
		private int id;
		public Minion(int id){
			this.id = id;
		}
		public void run(){
			try {
				Thread.sleep(r.nextInt(5000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Minion #"+id+ " reporting!");
			latch.countDown();
		}
	};
	
	public static void test() {
		
		for (int i = 0; i < 10; i++) {
			Thread t = new Minion(i);
			t.start();
		}
		
		try {
			latch.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("All minions reported!");
	}
	
	
}

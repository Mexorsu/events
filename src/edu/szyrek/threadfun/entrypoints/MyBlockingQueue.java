package edu.szyrek.threadfun.entrypoints;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MyBlockingQueue<T> {
	public static final int DEFAULT_MAX_ELEMS = 1;
	private final int MAX_ELEMS;
	private Queue<T> data = new LinkedList<T>();;
	private Lock dataLock = new ReentrantLock();
	private Condition isNotFull = dataLock.newCondition();
	private Condition isNotEmpty = dataLock.newCondition();

	public MyBlockingQueue(){
		MAX_ELEMS = DEFAULT_MAX_ELEMS;
	}
	
	public MyBlockingQueue(int maxElems){
		MAX_ELEMS = maxElems;
	}
	
	public boolean isEmpty() {
		dataLock.lock();
		boolean res = false;
		try {
			res = data.isEmpty();
		}catch (Exception e){
			res = false;
		}finally {
			dataLock.unlock();
		}
		return res;
		
	}
	
	public void clear() {
		dataLock.lock();
		boolean res = false;
		try {
			data.clear();
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			dataLock.unlock();
		}
	}
	
	public boolean isFull() {
		dataLock.lock();
		boolean res = false;
		try {
			return data.size() >= MAX_ELEMS ? true : false;
		}catch (Exception e){
			res = false;
		}finally {
			dataLock.unlock();
		}
		return res;
	}
	
	public void add(T elem){
		dataLock.lock();
		try {
			data.add(elem);
			isNotEmpty.signal();
		} catch (IllegalStateException e){
			throw new IllegalStateException("MyBlockingQueue is full");
		}finally{
			dataLock.unlock();
		}
	}
	
	public void put(T elem) throws InterruptedException{
		dataLock.lock();
		try {
			if (data.size()>=MAX_ELEMS) {
				isNotFull.await();
			}
			data.add(elem);
			isNotEmpty.signal();
		} catch (Exception e){
			throw new InterruptedException("Well, technically this shouldn't happen, but.. just in case..");
		}finally{
			dataLock.unlock();
		}
	}
	public T take() throws InterruptedException{
		dataLock.lock();
		try {
			if (data.size()==0) {
				isNotEmpty.await();
			}
			T res = data.remove();
			isNotFull.signal();
			return res;
		} catch (Exception e){
			throw new InterruptedException("Well, technically this shouldn't happen, but.. just in case..");
		}finally{
			dataLock.unlock();
		}
	}
}
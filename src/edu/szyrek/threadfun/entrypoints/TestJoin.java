package edu.szyrek.threadfun.entrypoints;

import edu.szyrek.util.CausedException;
import edu.szyrek.util.StringUtil;
import edu.szyrek.util.TestThread;


public class TestJoin {
    private static StringUtil su = StringUtil.getInstance();
    public static void test () {
    	System.out.println("--- TEST JOIN: ---");
        withoutJoin();
        try {
            Thread.sleep(2*TestThread.SLEEP_INTERVAL);
        } catch (Exception e) {
    
        }
        withJoin();
    }
    private static void withoutJoin() {
        su.printLine();
        System.out.println("Without join: ");
        Thread t = new TestThread("first");
        su.printLine();
        System.out.println("Before run");
        t.start();
        System.out.println("After run");
    }
    private static void withJoin() {
        su.printLine();
        System.out.println("With join: ");
        su.printLine();
        Thread t = new TestThread("second");
        System.out.println("Before run");
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            System.out.println(new CausedException(e).getRootCause());
        }
        System.out.println("After run");
    }
}

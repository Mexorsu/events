package edu.szyrek.threadfun.entrypoints;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Synchronized {
	private static int count = 0;
	private static Runnable noSync = new Runnable() {
		@Override
		public void run() {
			for (int i = 0; i < 10000; i++) {
				count++;
			}
		}
	};
	private static Runnable sync = new Runnable() {
		@Override
		public void run() {
			for (int i = 0; i < 10000; i++) {
				synchronized(this){
					count++;
				}
			}
		}
	};
	private static Runnable locked = new Runnable() {
		Lock l = new ReentrantLock();
		@Override
		public void run() {
			for (int i = 0; i < 10000; i++) {
				try {
					l.lock();
					count++;					
				}catch (Exception e){
					e.printStackTrace();
				}finally{					
					l.unlock();
				}
			}
		}
	};
	
	
	
	public static void notSynchronized() {
		long start_time = System.nanoTime();
		System.out.println("No synchronized version: ");
		count = 0;
		Thread t1 = new Thread(noSync);
		Thread t2 = new Thread(noSync);
		t1.start();
		t2.start();
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Result: "+count+" achieved in "+ (System.nanoTime()-start_time));
	}
	
	public static void synchronizedIndeed() {
		long start_time = System.nanoTime();
		System.out.println("Synchronized version: ");
		count = 0;
		Thread t1 = new Thread(sync);
		Thread t2 = new Thread(sync);
		t1.start();
		t2.start();
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Result: "+count+" achieved in "+ (System.nanoTime()-start_time));
	}
	
	public static void locked() {
		long start_time = System.nanoTime();
		System.out.println("Locks version: ");
		count = 0;
		Thread t1 = new Thread(locked);
		Thread t2 = new Thread(locked);
		t1.start();
		t2.start();
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Result: "+count+" achieved in "+ (System.nanoTime()-start_time));		
	}
	
    public static void test() {
    	notSynchronized();
    	synchronizedIndeed();
    	locked();
    }
}

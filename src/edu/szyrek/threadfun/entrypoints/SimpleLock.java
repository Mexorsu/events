package edu.szyrek.threadfun.entrypoints;

import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SimpleLock {
    static String[] sArray = new String[100];
    static Lock l = new ReentrantLock();
    static Condition notFull = l.newCondition();
    static Condition notEmpty = l.newCondition();
    static Random r = new Random();

    public static void test () {
        // Fill test array in half
        for (int i = 0; i < 50; i++) {
            sArray[i] = Integer.toString(i);
        }
        Thread inserter = new Thread(){
            public void run() {
                try {
                    l.lock();
                    if (sArray.length==100) notFull.await();
                    sArray[sArray.length] = Integer.toString(r.nextInt());
                    notEmpty.signal();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally{
                    l.unlock();
                }
                
            }
        };
        Thread remover = new Thread(){
            public void run() {
                try {
                    l.lock();
                    if (sArray.length==100) notEmpty.await();
                    sArray[sArray.length] = Integer.toString(r.nextInt());
                    notFull.signal();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally{
                    l.unlock();
                }
                
            }
        };
        while (true) {
            System.out.println("Array length is: " + sArray.length + ", last inserted is:"+sArray[sArray.length-1]);
            try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
}

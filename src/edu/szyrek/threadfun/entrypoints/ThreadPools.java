package edu.szyrek.threadfun.entrypoints;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class Proc implements Runnable {
	private int id;
	public Proc(int i){
		this.id = i;
	}
	public void run(){
		System.out.println("Starting "+id);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Ending "+id);
	}
}

public class ThreadPools {
	public static void test() {
		ExecutorService executor = Executors.newFixedThreadPool(5);
		for (int i = 0; i < 5; i++) {
			executor.submit(new Proc(i));
		}
		// Code continues, no waiting for executor to finish
		executor.shutdown();
		System.out.println("All tasks scheduled");
		try {
			executor.awaitTermination(1, TimeUnit.HOURS);
		} catch (InterruptedException e) {
			System.err.println("Some tasks failed");
			e.printStackTrace();
		}
		System.out.println("All tasks finished or timeout reached");
		
	}
}
